import type { Page, TestFixture } from '@playwright/test';
/**
 * Класс для реализации логики с рейтингом приложения котиков.
 *
 */
export class RatingPage {
  private page: Page;
  public tableSelector: string

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
  }


  async openRatingPage() {
    return await this.page.goto('/rating')
  }

  async returnLikes(raw: number) {
    const likes = await this.page.locator('[class="rating-names_item-count__1LGDH has-text-success"]').nth(raw).textContent();
    return Number(likes)
  }
}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};


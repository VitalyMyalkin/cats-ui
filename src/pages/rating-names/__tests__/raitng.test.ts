import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__'

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page }) => {
  await page.route(
    request => request.href.includes('https://meowle.fintech-qa.ru/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        status: 500,
      });
    }
  );
  await page.goto('/rating')
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible()
});

test('Рейтинг котиков отображается по убыванию', async ({ ratingPage   }) => {
  await ratingPage.openRatingPage()
  for (let raw = 0; raw < 9; raw++) { 
    await expect(await ratingPage.returnLikes(raw)).toBeGreaterThanOrEqual(await ratingPage.returnLikes(raw+1));
  }
    
});